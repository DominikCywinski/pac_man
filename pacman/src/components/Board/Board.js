import React, {Component} from 'react';

import Pacman from '../Pacman';
import Ghost from '../Ghost';
import Food from '../Food';
import './style.css';

class Board extends Component {

  constructor (props) {
    super(props);

    this.pacmanRef = React.createRef();
    this.ghostRef = React.createRef();

    this.foods = [];
    this.amountOfFood = (
      (window.innerWidth - this.props.foodSize)
      * (window.innerHeight - this.props.topScoreBoardHeight)
    ) / (this.props.foodSize * this.props.foodSize) - 1;

    for (let i = 0; i < this.amountOfFood; i++) {
      this['food' + i] = React.createRef();
    }
  }

  componentDidMount() {
    this.intervalFood = setInterval(this.lookForEat, 10);
    this.intervalCollision = setInterval(this.checkCollision, 10);
  }
  componentWillUnmount() {
    clearInterval(this.intervalFood);
    clearInterval(this.intervalCollision);
  }

  lookForEat = () => {

    const pacmanX = this.pacmanRef.current.state.position.left;
    const pacmanY = this.pacmanRef.current.state.position.top;
    const pacmanSize = this.pacmanRef.current.props.size;
    const pacmanLastX = pacmanX + pacmanSize / 2;
    const pacmanLastY = pacmanY + pacmanSize / 2;

    for (let i = 0; i <= this.amountOfFood; i++) {
      const currentFood = this['food' + i].current;
      if (currentFood) {
        const currentFoodX = currentFood.state.position.left;
        const currentFoodY = currentFood.state.position.top;
        const currentFoodSize = currentFood.props.foodSize;
        const currentFoodLastX = currentFoodX + currentFoodSize / 2;
        const currentFoodLastY = currentFoodY + currentFoodSize / 2;

        if (
          (pacmanX >= currentFoodX && pacmanX <= currentFoodLastX)
          || (pacmanLastX >= currentFoodX && pacmanLastX <= currentFoodLastX)) {
          if ((
            pacmanY >= currentFoodY && pacmanY <= currentFoodLastY
          ) || (pacmanLastY >= currentFoodY && pacmanLastY <= currentFoodLastY)) {
            if (!currentFood.state.hidden) {
              currentFood.ate();
              this.props.setScore((value) => value + 1);
            }
          }
        }
      }
    }
  }

  checkCollision = () => {
    if ((this.pacmanRef.current.state.position.left === this.ghostRef.current.state.position.left) &&
      (this.pacmanRef.current.state.position.top === this.ghostRef.current.state.position.top)) {
      this.props.setScore('LOSE');
      window.location.reload(false);
    }
  }

  render() {
    const {foodSize, border, topScoreBoardHeight} = this.props;
    let foods = [];
    let currentTop = 0;
    let currentLeft = 1 * foodSize;

    for (let i = 0; i < this.amountOfFood; i++) {
      if (currentLeft + this.props.foodSize >= window.innerWidth - border) {
        currentTop += foodSize;
        currentLeft = 0;
      }
      if (currentTop + foodSize >= (window.innerHeight - border - topScoreBoardHeight)) {
        break;
      }

      const position = {left: currentLeft, top: currentTop};
      currentLeft += foodSize;
      foods.push(
        <Food
          key={`food-elem-${ i }`}
          position={position}
          ref={this[`food` + i]}
        />
      )
    }

    return (
      <div className="board">
        {foods}
        <Pacman ref={this.pacmanRef} />
        <Ghost ref={this.ghostRef} color="red" />
      </div>
    )
  }
}

Board.defaultProps = {
  foodSize: 50,
  border: 20,
  topScoreBoardHeight: 50
}

export default Board;